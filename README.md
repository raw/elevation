# elevation #

A simple proof of concept for grabbing chunks of elevation data using the Google Maps API.

### Usage ###

* Open *elevation.html* in a web browser (tested on Firefox) and input two Latitude, Longitude corners of your choice.
* The number of samples that can be gathered on a single request is limited by Google.

### Contact ###

* River Allen <riverallen at gmail.com>