// This script grabs elevation data through the google maps api.
// Author: River Allen
// Copyright 2014

// Used for requesting elevation data from the google api.
var elevator;
// Element for printing out the lat,lng,elevation.
var buffer;
// Stores the google map.
var map;
// Stores all markers that are currently in use.
var markers = [];
// Stores all polygons that are currently in use.
var polygons = [];
// Stores form data, so it can be reloaded.
var localStorage;

// Retrieve the query string values from the url.
// http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript?lq=1
var urlParams;
(window.onpopstate = function () {
  var match,
  pl     = /\+/g,  // Regex for replacing addition symbol with a space
  search = /([^&=]+)=?([^&]*)/g,
  decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
  query  = window.location.search.substring(1);

  urlParams = {};
  while (match = search.exec(query))
    urlParams[decode(match[1])] = decode(match[2]);
})();


$(document).ready(function()
{
  elevator = new google.maps.ElevationService();
  buffer = $('#output-text');
  // Check if local storage is available.
  if ('localStorage' in window && window['localStorage'] !== null)
  {
    localStorage = window.localStorage;
    // If there is no local storage, use defaults. Otherwise, load the values
    // from local storage.
    if (localStorage.length > 0)
    {
      loadForms();
    }
  }


  // If there are full query strings process them.
  processQueryString();
});

function processQueryString()
{
  // Process the url query string if possible.
  if (typeof urlParams === 'undefined') return;

  try
  {
    var x1 = +urlParams['botLeftCornerLat'];
    var y1 = +urlParams['botLeftCornerLong'];
    var x2 = +urlParams['topRightCornerLat'];
    var y2 = +urlParams['topRightCornerLong'];
    var xResolution = +urlParams['samplesLat'];
    var yResolution = +urlParams['samplesLong'];
  }
  catch (e)
  {
    return;
  }
  if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) ||
      isNaN(xResolution) || isNaN(yResolution))
  {
    console
    return;
  }

  // Evaluate the query string for each checkbox
  // and fill the accompanying checkbox gui elements.
  $('input[type=checkbox]').each(function()
  {
    var checked = urlParams[this.name] !== undefined;
    $(this).prop('checked', checked);
  });

  // Get and output the elevation data.
  getElevationRect(x1, y1, x2, y2, xResolution, yResolution);
  $(window).load(function()
  {
    // Create markers on the map and center the map.
    var corner1 = new google.maps.LatLng(x1, y1);
    var corner2 = new google.maps.LatLng(x2, y2);
    addMarker(corner1);
    drawRect(corner1, corner2);
    centerMap(corner1);
  });
}

function initMap()
{
  // Load the map using some default position.

  var mapOptions = 
  {
    zoom: 3,
    center: new google.maps.LatLng(48.463, -123.311)
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

google.maps.event.addDomListener(window, 'load', initMap);

function getElevationRect(x1, y1, x2, y2, xResolution, yResolution)
{
  var width = x2 - x1;
  var height = y2 - y1;
  if ((xResolution * yResolution) >= 25000)
  {
    alert('The resolution specified would require more than 25000 requests.');
    return;
  }
  // Space between sample positions.
  var xSpacing = width / xResolution;
  var ySpacing = height / yResolution;

  var elevationPositions = [];

  for (var i = 0; i < xResolution; i++)
  {
    var xPoint = x1 + (xSpacing * i);
    for (var j = 0; j < yResolution; j++)
    {
      var yPoint = y1 + (ySpacing * j);
      elevationPositions.push(new google.maps.LatLng(xPoint, yPoint));
    }
  }
  
  // https://developers.google.com/maps/documentation/javascript/examples/elevation-simple
  var positionRequest = {
    'locations':elevationPositions
  }


  elevator.getElevationForLocations(positionRequest, 
                                    function(results, status) 
  {
    if (status == google.maps.ElevationStatus.OK)
    {
      temp = results;
      // Check if anything was found.
      if (results)
      {
        var csvData = [];
        if ($('input[name=addHead]').prop('checked'))
        {
          // Add heading for csv values.
          csvData.push('Latitude,Longitude,Elevation');
        }

        for (var i=0; i < results.length; i++)
        {
          var lat = results[i].location.lat();
          var lng = results[i].location.lng();
          var elv = results[i].elevation;
          csvData.push('' + lat + ',' + lng + ',' + elv + '\n');
        }
        displayArray(csvData);
      }
      else
      {
        println('No results found.');
      }
    }
    else
    {
      println('Elevation service failed due to: ' + status);
    }
  });
}

function addMarker(point)
{
  var marker = new google.maps.Marker({position:point, map:map});
  marker.setMap(map);
  markers.push(marker);
}

function drawRect(point1, point2)
{
  var corners = [
    point1,
    new google.maps.LatLng(point1.lat(), point2.lng()),
    point2,
    new google.maps.LatLng(point2.lat(), point1.lng()),
    point1,
  ];
  var rect = new google.maps.Polyline({
                        path: corners,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
  });
  rect.setMap(map);
  polygons.push(rect);
}

function centerMap(point, zoom)
{
  if (typeof(zoom)==='undefined') zoom = 12;
  map.setCenter(point);
  map.setZoom(zoom);
}

function displayArray(a)
{
  for (var i=0; i < a.length; i++)
  {
    println(a[i]);
  }
}

function println(s)
{
  buffer.html(buffer.html() + s + '<br>');
}

function clearOutput()
{
  clearDisplay();
  clearMap();
}

function clearDisplay()
{
  buffer.text('');
}

function clearMap()
{
  map.setZoom(3);
  for (var i=0; i < markers.length; i++)
  {
    markers[i].setMap(null);
  }
  markers = [];
  for (var i=0; i < polygons.length; i++)
  {
    polygons[i].setMap(null);
  }
  polygons = [];
}

function saveForms()
{
  if (localStorage)
  {
    $('input[type=checkbox]').each(function()
    {
      localStorage[this.name] = $(this).prop('checked');
    });
    $('input[type=text]').each(function()
    {
      localStorage[this.name] = $(this).val();
    });
  }
}

function loadForms()
{
  if (localStorage)
  {
    $('input[type=checkbox]').each(function()
    {
      $(this).prop('checked', localStorage[this.name]);
    });
    $('input[type=text]').each(function()
    {
      $(this).val(localStorage[this.name]);
    });
  }
}
